import models.Circle;
import models.Rectangle;
import models.Shape;
import models.Square;

public class SuperShapeClass{
    public static void main(String[] args) throws Exception {
        Shape shape1 = new Shape();
        System.out.println("Shape 1:");
        System.out.println(shape1);
        System.out.println("----------------------------------------------");

        Shape shape2 = new Shape("Green", false);
        System.out.println("Shape 2:");
        System.out.println(shape2);
        System.out.println("----------------------------------------------");


        Circle Circle1 = new Circle();
        System.out.println("Circle 1:");
        System.out.println(Circle1);
        System.out.println("----------------------------------------------");


        Circle Circle2 = new Circle(2.0);
        System.out.println("Circle 2:");
        System.out.println(Circle2);
        System.out.println("----------------------------------------------");

        Circle Circle3 = new Circle(2.0, "green", true);
        System.out.println("Circle 3:");
        System.out.println(Circle3);
        System.out.println("----------------------------------------------");

        Rectangle rectangle1 = new Rectangle();
        System.out.println("Rectangle 1:");
        System.out.println(rectangle1);
        System.out.println("----------------------------------------------");


        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        System.out.println("Rectangle 2:");
        System.out.println(rectangle2);
        System.out.println("----------------------------------------------");

        Rectangle rectangle3 = new Rectangle(2.0, 1.5, "Green", true);
        System.out.println("Rectangle 3:");
        System.out.println(rectangle3);
        System.out.println("----------------------------------------------");


        Square square1 = new Square(0);
        System.out.println("Square 1");
        System.out.println(square1);
        System.out.println("----------------------------------------------");

        Square square2 = new Square(1.5);
        System.out.println("Square 2");
        System.out.println(square2);
        System.out.println("----------------------------------------------");

        Square square3 = new Square(2.0, "Green", true);
        System.out.println("Square 3");
        System.out.println(square3);
        System.out.println("----------------------------------------------");

    }
}
